from unittest import TestCase

from factory_alchemist import factory
from factory_alchemist.generators import rand_word
from tests.dummy_models import Json, Jam
from . import BaseTest
from .dummy_models import Session, Spam, Ham, BaseModel, FishTable


factory.BaseModel = BaseModel


class MakeTest(BaseTest):
    def setUp(self):
        self.s = Session()

    def test_create_record_with_default_values_for_non_nullable_columns(self):
        factory.make(self.s, Spam)

        spam = self.s.query(Spam).first()
        self.assertEqual(spam.id, 1)
        self.assertEqual(spam.purpose, None)
        self.assertEqual(spam.brand, 'biz')
        self.assertEqual(spam.created_at, 'foo bar')
        self.assertIsInstance(spam.flavor, int)

    def test_create_record_with_specified_values(self):
        factory.make(self.s, Spam, id=9, purpose='Nothing')
        self.assertEqual(self.s.query(Spam.id, Spam.purpose).all(), [(9, 'Nothing')])

    def test_create_non_nullable_relationships(self):
        factory.make(self.s, Ham)

        self.assertEqual(self.s.query(Ham.id, Ham.spam_id).all(), [(1, 1)])
        self.assertEqual(self.s.query(Spam.id).all(), [(1,)])

    def test_dont_overwrite_non_nullable_field(self):
        factory.make(self.s, Ham, spam_id=99)
        self.assertEqual(self.s.query(Ham.id, Ham.spam_id).all(), [(1, 99)])

    def test_fill_non_nullable_field_with_its_default_value(self):
        spam = factory.make(self.s, Spam)
        self.assertEqual(spam.brand, 'biz')


class CustomTypeGeneratorTest(TestCase):
    def setUp(self):
        self.s = Session()

    def test_generate_value_for_custom_type(self):
        factory.add_type(Json, lambda type_: {rand_word(10): rand_word(10)})
        jam_details = factory.make(self.s, Jam).details

        self.assertIsInstance(jam_details, dict)


class MakeFromTableTest(BaseTest):
    def test_create_from_table_object(self):
        fish_1 = factory.make_t(FishTable, id=5)
        fish_2 = factory.make_t(FishTable, id=11)

        self.assertItemsEqual(list(FishTable.select().execute()), [(5, 1), (11, 2)])

        self.assertEqual(fish_1.id, 5)
        self.assertEqual(fish_1.spam_id, 1)
        self.assertEqual(fish_2.id, 11)
        self.assertEqual(fish_2.spam_id, 2)
