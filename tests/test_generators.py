from unittest import TestCase
from datetime import datetime, date
from sqlalchemy import Enum, DateTime, Date, Boolean, CHAR, String, Float, BigInteger, Integer, SmallInteger
from factory_alchemist.generators import _generate_value


class IntegerValueGeneratorTest(TestCase):
    def test_generate_random_value_for_smallint(self):
        for i in range(100):
            self.assertIn(_generate_value(SmallInteger()), [0, 1])

    def test_generate_random_value_for_integer(self):
        for i in range(100):
            value = _generate_value(Integer())
            self.assertGreaterEqual(value, 0)
            self.assertLessEqual(value, 2147483647)

    def test_generate_random_value_for_bigint(self):
        for i in range(100):
            value = _generate_value(BigInteger())
            self.assertGreaterEqual(value, 0)
            self.assertLessEqual(value, 9223372036854775807)


class FloatValueGeneratorTest(TestCase):
    def test_generate_random_float(self):
        value = _generate_value(Float())
        self.assertIsInstance(value, float)

    def test_generate_random_float_between_in_interval(self):
        for _ in range(100):
            value = _generate_value(Float())

            self.assertGreaterEqual(value, 0.0)
            self.assertLessEqual(value, 99999.0)


class StringValueGeneratorTest(TestCase):
    def test_generate_random_value_for_string(self):
        self.assertIsInstance(_generate_value(String()), str)

    def test_generate_random_value_for_string_with_max_chars(self):
        self.assertEquals(5, len(_generate_value(String(5))))


class CharValueGeneratorTest(TestCase):
    def test_generate_random_value_for_char(self):
        self.assertIsInstance(_generate_value(CHAR(30)), str)

    def test_generate_random_value_for_string_with_max_chars(self):
        self.assertEqual(28, len(_generate_value(CHAR(28))))


class BooleanValueGeneratorTest(TestCase):
    def test_generate_random_value_for_boolean(self):
        generated_values = {_generate_value(Boolean()) for val in range(100)}
        self.assertEquals(generated_values, {True, False})


class DateValueGeneratorTest(TestCase):
    def test_generate_random_value_between_dates(self):
        for i in range(1000):
            value = _generate_value(Date())
            self.assertTrue(value >= date(1950, 1, 1))
            self.assertTrue(value < date(2050, 12, 31))

    def test_generate_date_type(self):
        self.assertIsInstance(_generate_value(Date()), date)


class DateTimeValueGeneratorTest(TestCase):
    def test_generate_random_value_between_datetimes(self):
        for i in range(100):
            value = _generate_value(DateTime())
            self.assertTrue(value > datetime(1950, 1, 1, 0, 0, 0))
            self.assertTrue(value < datetime(2050, 12, 31, 23, 59, 59))

    def test_generate_datetime_type(self):
        self.assertIsInstance(_generate_value(DateTime()), datetime)


class EnumValueGeneratorTest(TestCase):
    def test_generate_list_of_strings(self):
        for _ in range(100):
            value = _generate_value(Enum('ham', 'spam'))
            self.assertIn(value, ['ham', 'spam'])
