import json

from sqlalchemy import create_engine, MetaData, Column, Integer, String, \
    ForeignKey, Table, TypeDecorator
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


engine = create_engine('sqlite:///')
metadata = MetaData(bind=engine)
BaseModel = declarative_base(metadata=metadata)
Session = sessionmaker(bind=engine, autoflush=True, autocommit=False)


class Json(TypeDecorator):
    impl = String

    def process_bind_param(self, value, dialect):
        return json.dumps(value)

    def process_result_value(self, value, dialect):
        return json.loads(value)


class Spam(BaseModel):
    __tablename__ = 'spam'

    id = Column(Integer, primary_key=True)
    purpose = Column(String)
    flavor = Column(Integer, nullable=False)
    brand = Column(String, default='biz', nullable=False)
    created_at = Column(String, default=lambda: 'foo bar', nullable=False)


class Ham(BaseModel):
    __tablename__ = 'ham'

    id = Column(Integer, primary_key=True)
    spam_id = Column(Integer, ForeignKey('spam.id'), nullable=False)


class Jam(BaseModel):
    __tablename__ = 'jam'

    id = Column(Integer, primary_key=True)
    details = Column(Json, nullable=False)


FishTable = Table('fish', metadata,
                  Column('id', Integer, primary_key=True),
                  Column('spam_id', Integer, ForeignKey('spam.id'), nullable=False))
